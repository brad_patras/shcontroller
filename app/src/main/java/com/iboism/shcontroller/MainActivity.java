package com.iboism.shcontroller;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.lantouzi.wheelview.WheelView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements WheelView.OnWheelItemSelectedListener {

    public final String POST_HUMIDITY_SETTING = "/change_humidity_setting";
    public final String GET_ALL_SETTINGS = "/all_settings";
    public final String POST_HUMIDITY_STATE = "/user_state";
    public final String GET_RASPBERRY_IP = "http://arlenburroughs.com/extras/486/get_addr.php?key=jkoplkjasd908f7f7f7fa89sd7jh34j";
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    String baseUrl;
    String currentHumidityString;
    int currentSetting;
    boolean humidifierOn;

    WheelView wheelPicker;
    RelativeLayout loadingView;
    OkHttpClient client = new OkHttpClient();
    Handler refreshHandler;
    Handler mainHandler;
    Thread refreshThread;
    TextView currentHumidity;
    ToggleButton powerButton;

    /**
     * Runnable to be run on a background thread, retrieves all the humidifier state variables and then notifies the
     * UI to update the views
     */
    Runnable refresh = new Runnable() {

        @Override
        public void run() {

            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

            try {
               getAllSettings(baseUrl + GET_ALL_SETTINGS);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mainHandler.post(updateUI);
            refreshHandler.postDelayed(refresh, 30000);
        }
    };

    /**
     * Runnable that updates the UI views after the state variables have been retrieved from the server
     */
    Runnable updateUI = new Runnable() {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    currentHumidity.setText(currentHumidityString );
                    wheelPicker.selectIndex(currentSetting);
                    powerButton.setChecked(humidifierOn);
                    loadingView.setVisibility(View.GONE);
                }
            });

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*
        Initialize views
         */
        wheelPicker = (WheelView) findViewById(R.id.wheel_picker);
        powerButton = (ToggleButton) findViewById(R.id.power_button);
        currentHumidity = (TextView) findViewById(R.id.curr_h);
        loadingView = (RelativeLayout) findViewById(R.id.loading_view);

        /*
        Prepare threads and handlers
         */
        refreshThread = new Thread(refresh);
        refreshHandler = new Handler();
        mainHandler = new Handler(Looper.getMainLooper());

        /*
        build http request client
         */
        client = new OkHttpClient.Builder()
                .connectTimeout(35, TimeUnit.SECONDS)
                .writeTimeout(35, TimeUnit.SECONDS)
                .readTimeout(35, TimeUnit.SECONDS)
                .build();

        /*
        build array of values from 0-100 for wheel picker
         */
        ArrayList<String> vals = new ArrayList<>();
        for (int i = 0; i <= 100; i++) {
            vals.add(i, Integer.toString(i));
        }

        /*
        setup wheel picker
         */
        assert wheelPicker != null;
        wheelPicker.setOnWheelItemSelectedListener(this);
        wheelPicker.setMinSelectableIndex(0);
        wheelPicker.setMaxSelectableIndex(100);
        wheelPicker.setItems(vals);


        /*
        setup onclick event for the power button
         */
        powerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                humidifierOn = powerButton.isChecked();
                new Thread(new Runnable(){
                    @Override
                    public void run() {
                        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
                        try {
                            postHumidityState(baseUrl + POST_HUMIDITY_STATE, humidifierOn ? "on" : "off");
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*
        get the humidifier state variables
         */
        try {
            baseUrl = "http://" + getRaspberryIp(GET_RASPBERRY_IP) + ":5000";

            if (refreshThread.getState() == Thread.State.NEW){
                refreshThread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * Method for getting the raspberry pi's public IP Address
     * @param url The url where the Raspberry pi is hosting it's address
     * @return the raspberry pi's IP address
     * @throws IOException if no connection is available
     * @throws JSONException if the response is malformed
     */
    private String getRaspberryIp(String url) throws IOException, JSONException {
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = client.newCall(request).execute();
        JSONObject r = new JSONObject(response.body().string());
        return r.getString("addr");
    }

    /**
     *
     * @param url the url pathway for setting the humidity
     * @param setting the desired humidity setting
     * @throws IOException if there was a connection error
     * @throws JSONException if the response is malformed
     */
    private void postHumiditySetting(String url, int setting) throws IOException, JSONException {
        URL addr = new URL(url + "/" + setting);
        Log.i("url", addr.toString());
        JSONObject j = new JSONObject();
        j.put("value", setting);
        RequestBody body = RequestBody.create(JSON, j.toString());
        Request request = new Request.Builder()
                .url(addr)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        Log.i("response", response.body().string());

    }

    /**
     * Turning the humidifier on and off manually
     * @param url url for posting a new humidifier state
     * @param state 'on' or 'off'
     * @throws IOException connection error
     * @throws JSONException malformed response
     */

    private void postHumidityState(String url, String state) throws IOException, JSONException {
        RequestBody body = RequestBody.create(JSON, state);
        URL addr = new URL(url + "/" + state);
        Request request = new Request.Builder()
                .url(addr)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        JSONObject r = new JSONObject(response.body().string());

    }

    /**
     * Get the current state variables of the humidifier
     * @param url url for the retrieving the state
     * @throws IOException connection error
     * @throws JSONException malformed response
     */
    private void getAllSettings(String url) throws IOException, JSONException {
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = client.newCall(request).execute();

        JSONObject r = new JSONObject(response.body().string());
        String state = r.getString("current_user_state");

        humidifierOn = state.equalsIgnoreCase("on");
        currentHumidityString = "" + r.getInt("current_humidity");
        currentSetting = r.getInt("current_humidity_setting");

    }

    @Override
    public void onWheelItemChanged(WheelView wheelView, int position) {

    }

    /**
     * When a new setting is selected with the wheel picker view
     * @param wheelView the wheelview object
     * @param position the selected position
     */
    @Override
    public void onWheelItemSelected(WheelView wheelView, int position) {
        final int p = position;
        new Thread(new Runnable() {
            @Override
            public void run() {
                android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
                try {
                    String json = "value :" + p;
                    Log.i("json", json);
                    postHumiditySetting(baseUrl + POST_HUMIDITY_SETTING, p);

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}

